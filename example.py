from database import db

"""example.py module
available methods from database.py module:
1) get_by_id
2) get
3) get_all
4) execute_and_fetch

For Example:

1) db.get_by_id(id, select=YOUR_SELECT_STRING, table=TABLE_NAME, as_dict=True) # return one dictionary
2) db.get(where, select=YOUR_SELECT_STRING, table=TABLE_NAME,  as_dict=False) # return one dictionary
3) db.get_all(where, select=YOUR_SELECT_STRING, table=TABLE_NAME,  as_dict=False) # return list of dictionary
recommended:
4) db.execute_and_fetch(sql_command, as_dict=True) # return list of dictionary

"""

def foo(id):
  command = """
        SELECT user.id, user.email, project.*
        FROM project
        JOIN user ON project.user_id = user.id
        WHERE user.id = {user_id};
      """.format(user_id=id)
  res = db.execute_and_fetch(command, as_dict=True)
  return res


if __name__ == '__main__':
  res = foo(47)
  for item in res:
   print(item)