#
import MySQLdb as mysql
import config

def db_connect():
  connect = mysql.connect(
    host=config.DATABASE_HOST, db=config.DATABASE_NAME, 
    user=config.DATABASE_USER, passwd=config.DATABASE_PASSWORD,
    charset=config.DATABASE_CHARSET
  )
  return connect

class AS_IS(object):
  def __init__(self, value):
    self.value = value

class BaseStorage(object):
  AS_IS = AS_IS
  TABLE_NAME = None
  ID_FIELD = 'id'
  PARAM_CHAR = '%s'

  def __init__(self, table=None, id_field=None, connect=None):
    self._connect = None
    self._cursor  = None
    self._table = table if table else self.TABLE_NAME
    self._id_field = id_field if id_field else self.ID_FIELD
    self.reconnect(connect)

  id_field = property(lambda self: self._id_field)

  def _close_cursor(self):
    self._cursor.close()
    self._cursor = None

  def _execute(self, *args, **kwargs):
    try:
      self._cursor.execute(*args, **kwargs)
    except Exception as error:
      if repr(error).find('InterfaceError') >= 0:
        self._cursor = self._connect.cursor()
        self._cursor.execute(*args, **kwargs)
      else:
        raise error

  def reconnect(self, connect=None):
    if connect is None:
      connect = db_connect()
    self._connect = connect
    self._cursor  = self._connect.cursor()

  def _prepare_where(self, where):
    try:
      basestring
    except NameError:
      basestring = str
    where_array = []; where_values = []
    if where:
      if   isinstance(where, dict):
        where_array = []
        for name, value in where.items():
          name = name.strip()
          end_char = name[-1:]
          if end_char not in ( '=', '<', '>' ):
            w1 = "%s = %s" % (name, self.PARAM_CHAR)
          else:
            w1 = "%s %s" % (name, self.PARAM_CHAR)
          where_array.append(w1)
          where_values.append(value)
        where = " AND ".join(where_array)
      elif not isinstance(where, basestring):
        where = None
    else:
      where = None
    return (where, where_values)

  def _prepare_join(self, join):
    if isinstance(join, str):
      join = 'JOIN %s' % join
    else:
      join_array = []
      for item in join:
        if isinstance(join, str):
          item = 'JOIN %s' % item
        else:
          item = 'JOIN %s ON %s' % tuple(item)
        join_array.append(item)
      join = '\n'.join(join_array)
    return join

  def _prepare_left_join(self, join):
    if isinstance(join, str):
      join = 'LEFT JOIN %s' % join
    else:
      join_array = []
      for item in join:
        if isinstance(join, str):
          item = 'LEFT JOIN %s' % item
        else:
          item = 'LEFT JOIN %s ON %s' % tuple(item)
        join_array.append(item)
      join = '\n'.join(join_array)
    return join

  def _prepare_dict_items(self, data, cursor):
    descriptions = [ desc[0] for desc in cursor.description ]
    res = []
    for item in data:
      # item2 = dict([
      #   (name, value) for name, value in map(None, descriptions, item)
      # ])
      res.append(dict(zip(descriptions, item)))
      # res.append(item2)
    return res

  def _prepare_dict_item(self, data, cursor):
    if data:
      # res = dict([
      #   (desc[0], value) for desc, value in map(None, cursor.description, data)
      # ])
      res = dict([
        (desc[0], value) for desc, value in zip(cursor.description, data)
      ])
    else:
      res = None
    return res

  def close(self):
    self._close_cursor()
    self._connect.close()
    self._connect = None

  def with_transaction(self, function, *args, **kwargs):
    self.begin()
    error = None
    try:
      res = function(*args, **kwargs)
    except Exception as error:
      print(error)
      self.rollback()
      raise error
    else:
      self.commit()
    return res

  def execute(self, command, values=None):
    if values is not None:
      self._execute(command, values)
    else:
      self._execute(command)
    return self._cursor

  def execute_and_fetch(self, command, values=None, as_dict=False):
    cursor = self.execute(command, values)    
    res = cursor.fetchall()
    if res and as_dict:
      res = self._prepare_dict_items(res, cursor)
    return res

  def begin(self):
    self._execute('BEGIN')

  def commit(self):
    self._execute('COMMIT')

  def rollback(self):
    self._execute('ROLLBACK')

  def get_by_id(self, id, select=None, table=None, as_dict=True, debug=False):
    return self.get({ self._id_field: id }, select, table=table, as_dict=as_dict, debug=debug)

  def get(self, where, select=None, order_by=None, join=None, table=None,  as_dict=False):
    if select is None:
      select = '*'
    table = table if table else self._table
    command = "SELECT %s FROM %s" % (select, table)
    where, where_values = self._prepare_where(where)
    if join:
      join = self._prepare_join(join)
      command += '\n %s' % join
    if where:
      command += " WHERE %s" % where
    if order_by:
      command += " ORDER BY %s" % order_by
    command += " LIMIT 1"
    self._execute(command, where_values)
    res = self._cursor.fetchone()
    if res and as_dict:
      res = self._prepare_dict_item(res, self._cursor)
    return res

  def get_all(self, where=None, select=None, order_by=None, group_by=None, limit=None, join=None,
    left_join=None, table=None, as_dict=False
  ):
    table = table if table else self._table
    if select is None:
      select = '*'
    command = "SELECT %s FROM %s" % (select, table)
    where, where_values = self._prepare_where(where)
    if join:
      join = self._prepare_join(join)
      command += '\n %s' % join
    if left_join:
      left_join = self._prepare_left_join(left_join)
      command += '\n %s' % left_join
    if where:
      command += " WHERE %s" % where
    if order_by:
      command += " ORDER BY %s" % order_by
    if group_by:
      command += " GROUP BY %s" % group_by
    if limit:
      command += " LIMIT %s" % limit
    return self.execute_and_fetch(command, where_values, as_dict=as_dict)


db = BaseStorage()
